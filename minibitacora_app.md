# Mini Bitacora de Obra
---
*El programa ya casi todo funciona desde la misma app, pero puedes mirar la carpeta "CALLE_EJEMPLAR" como un ejemplo*

Al instalar el programa y dar los permisos necesarios se crea una carpeta en la memoria interna llamada
"ProjectLogbook", dentro de esta carpeta se encuentra toda la informacion generada y/o necesaria para el programa.

Para crear una obra nueva puedes hacerlo ahora desde la aplicacion (esto generara todas las carpetas e informacion
necesaria dentro de una carpeta con el mismo nombre en mayusculas dentro de ProjectLogbook).

Entonces cada carpeta dentro de "ProjectLogbook" representa una obra a la que se haran notas.

Cada carpeta dentro de "ProjectLogBook" esta conformada de la siguiente manera:
* Carpeta Principal indicando el nombre del proyecto (deberan ser nombres unicos no repetidos).
    Dentro de cada carpeta se deben/pueden encontrar los siguientes archivos (los nombres deben ser
    tal cual se indica a continuacion)
    * InformacionGeneral.txt
        Archivo opcional de texto plano en donde se puede escribir cualquier informacion de la obra
        que se quiera revisar en algun momento, el contenido de este archivo se puede observar y editar
        en el menu desplegable "InformacionGeneral"
    * special_notes.json
        Archivo opcional con formato JSON que indica notas "tipo" a las cuales se accede con el boton
        de "+" de nota nueva (este archivo ahora se crea/modifica/eliminar dentro de la aplicacion 
        apretando el boton "ADMINISTRAR NOTAS").
        Los datos dentro de este archivo conforman el siguiente objeto JSON.

            {"special_notes": {
                "TITULO DE LA NOTA": {
                "body": "CUERPO TIPO DE LA NOTA",
                "default_to": "PARTIDA/NOMBRE EN LA QUE SE AGREGARAN DICHOS TIPOS DE NOTAS"},

                ... MAS NOTAS
            }}

        En donde se destacan los siguientes atributos/propiedades:
        "TITULO DE LA NOTA" -> Deberan ser unicos indicando que tipo de nota "tipo" es
        "body" -> Es de caracter opcional y en caso de existir sirve de plantilla para llenar notas (pueden
                  usarse caracteres especiales como saltos de linea \n u otro tipo segun JAVA) https://docs.oracle.com/javase/tutorial/java/data/characters.html
        "default_to" -> Es de caracter opcional y en caso de existir indica la categoria/partida en el que
                        seran guardadas las notas, en caso de no existir entonces se guardaran en la categoria/partida
                        de la que fue apretado el boton "+"

    * catalogo.csv
        Archivo opcional (sin este archivo el programa no puede mostrar nada en la pestaña de "Catalogo")
        que debe contener el catalogo de la obra en formato csv con la siguiente estructura:

            PARTIDA, CLAVE, DESCRIPCION, UNIDAD, CANTIDAD_DE_CONTRATO, PRECIO_UNITARIO
            
                                 E  J  E  M  P  L  O
            EXCAVACIONES, C.00.001, EXCAVACION A MANO DE 0-2, M3, 250.0, 130.58
            EXCAVACIONES, C.00.002, EXCAVACION CON MAQUINA DE 0-2, M3, 180.0, 180.0
            PRELIMINARES, C.00.003, TRAZO Y NIVELACION, M2, 1600.0, 5.84

        Puedes ver un ejemplo dentro de la carpeta CALLE_EJEMPLAR.



## ARCHIVOS GENERADOS POR EL PROGRAMA 
Una vez que empezaste a utilizar el programa, se generan (dentro de la carpeta que representa la obra elegida)
los siguientes archivos:

*   Carpeta "NotasPictures":
    Aqui se guardan las fotos que tomas dentro de cada nota (si se borra alguna o todas las fotos de esta carpeta
    no se observaran en la aplicacion).
    Se recomienda no borrar estas fotos hasta que se haya terminado la obra y se vaya a copiar la carpeta completa
    a un lugar seguro (tu computadora por ejemplo).
*   Carpeta "Pictures":
    Aqui se guardan de manera organizada las fotos que tomas desde la aplicacion utilizando el simbolo de la camara
    sin que sea dentro de una obra, esta carpeta solo sirve para de alguna manera tener organizadas las fotos
    tomadas desde la aplicacion (se recomienda ir copiando esta carpeta de vez en cuando a un lugar seguro como
    tu computadora por ejemplo y vaciar las carpetas de la memoria interna ya que estas ocupan espacio en tu celular).
    Por ejemplo, cada semana copiar la carpeta "Pictures" a la computadora y borrarla de la memoria interna del celular.
*   Archivo "obrasAPP.sqlite":
    Este archivo ahora solo se crea una vez que decides "exportar obra" desde la aplicacion y puede ser utilizado
    en otro celular con la aplicacion al utilizar "importar obra".
    Es entonces la base de datos de cada obra (solo util para exportar e importar) solo contiene las notas de la obra
    particular (ya no se guarda el catalogo dentro de la base de datos como en la aplicacion anterior).
    Al finalizar la supervision de obra, se puede utilizar este archivo para por ejemplo imprimir todas las notas tomadas
    durante la obra y guardarlas de manera fisica en alguna carpeta en tu oficina (utilizando software adicional especializado
    para eso).
*   Archivo "obrasAPP.sqlite-journal":
    Archivo creado por el sistema operativo Android para llevar un control de la version y otros datos de las bases
    de datos utilizadas (se recomienda tampoco borrarlo porque puede afectar el funcionamiento de la aplicacion).
*   Archivo "app.data":
    Este archivo puedes encontrarlo solo en la version anterior de la aplicacion ya que en esta nueva version los datos
    guardados previamene en este archivo, se guardan dentro de la base de datos de la aplicacion.
*   Carpeta "Reportes":
    Aqui se guardan todos los reportes (arhivos HTML) que creas desde la aplicacion, los reportes estan pensados para
    ser impresos desde una computadora utilizando tu buscador-web favorito (se probo solamente en firefox y google-chrome).
    Es necesario copiar la carpeta "NotasPicture" y conservar la jerarquia de folders de la aplicacion, ya que los reportes.html
    buscan las fotos directamente en esa carpeta "../NotasPictures/nombre-de-foto" por lo que se recomienda copiar toda
    la carpeta de la obra al momento de querer revisar los reportes en tu computadora.
    Otra cosa importante es que la aplicacion no comprime o reduce calidad de las fotos, por lo que los reportes pueden ser
    demasiado pesados (se recomienda etonces que para utilizar la aplicacion no se utilice la camara con toda la resolucion
    posible si no mas bien bajarla para no tener problema con esto).

Espero que le encuentres alguna utilidad a este programa :)
