# NIVELES CON ANDROID
---
Aplicacion para ayudar en nivelaciones realizadas en campo, permite:

* ORGANIZAR TUS TRABAJOS POR OBRAS
* IMPORTAR BANCOS DE NIVEL, REPLANTEOS Y OBRAS COMPLETAS CREADAS EN LA APP.
* EXPORTAR DATOS DE BANCOS, LEVANTAMIENTOS, REPLANTEOS Y OBRAS COMPLETAS
  CREADAS EN LA APP.

Se genera una carpeta en la memoria interna del celular con la siguiente
estructura:

NivelacionAPP
 * EXPORTS (Datos guardados desde la APP)
   BancosDeNivel -> Se salva por obra los bancos de nivel exportados mediante
                    la app.
   BasesDeDatos -> Se salva las obras exportadas mediante la app (pueden ser
                   copiadas a la carpeta IMPORTS/BasesDeDatos para importar
		   en algun otro celular con la aplicacion.
   Levantamientos -> Se salvan por obra los levantamientos exportados mediante
                     la app.
   Replanteos -> Se salvan por obra los replanteos exportados mediante la app.
   
 * IMPORTS (Datos que pueden ser importados hacia la APP)
   BancosDeNivel -> Se crea una carpeta por obra en la cual se pueden guardar
                    archivos .csv delimitados por coma (De preferencia solo
		    caracteres ASCII).
		    El archivo en cada carpeta debe llamarse "BancosDeNivel.csv"
		    y la estructura del mismo debe ser como la siguiente.
		    LA PRIMER FILA FORSOZAMENTE DEBE LLEVAR "BANCO, ALTURA".

                    BANCO, ALTURA
		    nombre1, altura1
		    nombre2, altura2
		    nombren, alturan
 		    ..., ...
    * BasesDeDatos -> Puedes guardar las bases de datos NOMBRE.sqlite generadas
                   por la misma app y guardadas en la carpeta EXPORTS/BasesDeDatos
                   deberan ser nombres unicos para no crear conflictos.
    * Replanteos -> Se crea una carpeta por obra en la cual se pueden guardar
   	      	 archivos .csv delimitados por coma (De preferencia solo
            caracteres ASCII).
            Los archivos deben tener nombre unico el cual sera utilizado para
            el nombre del replanteo en la aplicacion.
            Y la estructura de cada archivo debe ser la siguiente (ejemplos).

            Archivo con nombre: "POZOS.csv" -> El replanteo se llamara POZOS
            NUMERO_DE_POZO, FONDO,   TAPA           
            1,		 1800.1,  1801.150
            2,		 1700.10, 1701.15

            NUMERO_DE_POZO -> Sera el Id asociado a cada punto en la app.
            FONDO, TAPA -> sera la cantidad de puntos que tendra cada id
                        la cantidad de puntos que se replantearan.
            1,2,3 ..    -> sera la cantidad de elementos que se replantearan.

            Archivo con nombre: "RASANTES.csv" -> El replanteo se llamara RASANTES.
            CADENAMIENTO, IZQUIERDA, CENTRO, DERECHA
            0+00,	      100,	 100.1,	 100.2
            0+010,	      100.10,	 100.15, 100.33

            CADENAMIENTO -> sera el Id asociado a cada punto en la app.
            IZQUIERDA, CENTRO, DERECHA -> sera la cantidad de puntos que tendra
                        cada id (la cantidad de puntos a replantear)
					   
