# Optical Level Notebook for Topography
---
Topographic levelling booking tool for helping with daily jobs, this app helps you by:

* LETTING YOU MANAGE YOUR DIFFERENTE PROJECTS YOU HAVE.
* IMPORT BENCHMARK, STAKEOUT AND WHOLE PROJECTS FROM WITHIN YOUR APP.
* EXPORT BENCHMARK, SURVEYS AND STAKEOUT DATA TO CSV FORMAT.

It creates this internal file structure in your celphone:

NivelacionAPP
 * EXPORTS (saved data created by app)
   BancosDeNivel -> Saved by project, its all benchmark data exported from the app.
   BasesDeDatos -> Sqlite files saved from the app so you can later import them into 
                   celphones with this app by copying them into IMPORTS/BasesDeDatos.
   Levantamientos -> Saved by project, all surveys you export from your app.
   Replanteos -> Saved by project, all stakeout data you export from your app.
   
 * IMPORTS (data that can be imported into your app)
   BancosDeNivel -> folder where you can save .csv files (onli ASCII) to later import benchmar 
                    dat into your app.
		    File you import most be named "BancosDeNivel.csv"
		    and the structure most follow the next requirements.
		    FIRS FILE MOST BE "BANCO, ALTURA"

                    BANCO, ALTURA
		    benchmark-name1, benchmark-height1
		    benchmark-name2, benchmark-height2
		    benchmark-nameN, benchmark-heightN
 		    ..., ...
    * BasesDeDatos -> Here you can save ProjectName.sqlite files created by you app (when exported)
                   you need to check that name is unique or will cause conflicts otherwhise.
    * Replanteos -> It generates different projects folders so you can import stakeout data into 
                 your app. Here you save .csv files (ASCII only).
		 
		 Files most be unique and name will be used to name your stakeout survey in your app.
		 files structure most follow next requirements (examples).

            file named: "WELLS.csv" -> El replanteo se llamara WHELLS
            WHELL_NUMBER, BOTTOM-HEIGHT,   TOP-HEIGHT           
            1,		 1800.1,  1801.150
            2,		 1700.10, 1701.15

            WHELL_NUMBER -> This one is Id associated which each point in the app.
            BOTTOM-HEIGHT, TOP-HEIGHT -> will be number of points that need to be stakeout for every id.
            1,2,3 ..    -> points that need to be checked in your stakeout survey.
